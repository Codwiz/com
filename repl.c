#include <stdio.h>
#include <stdlib.h>
#include <readline/readline.h>
#include <readline/history.h>

#include "agent.h"
#include "builtin_modules.h"
#include "kosix.h"
#include "compiler.h"
#include "disassemble.h"
#include "eval.h"
#include "intrinsics.h"
#include "module.h"

typedef struct {
  cb_bytecode *bytecode;
  cb_compile_state *compile_state;
  cb_modspec *modspec;
  int did_init_vm;
  size_t pc;
} REPLContext;

void initializeVM(REPLContext *context) {
  context->did_init_vm = 1;
  cb_vm_init(context->bytecode);
  struct cb_module *mod = &cb_vm_state.modules[cb_modspec_id(context->modspec)];
  mod->spec = context->modspec;
  mod->global_scope = cb_hashmap_new();
  cb_instantiate_builtin_modules();
  make_intrinsics(mod->global_scope);
}

void initializeCompileState(REPLContext *context, const char *line) {
  context->pc = cb_bytecode_len(context->bytecode);
  cb_compile_state_reset(context->compile_state, line, context->modspec);
}

void repl() {
  REPLContext context;
  context.bytecode = cb_bytecode_new();
  context.modspec = cb_modspec_new(cb_agent_intern_string("<repl>", 6));
  context.compile_state = cb_compile_state_new("<repl>", context.modspec, context.bytecode);
  context.did_init_vm = 0;

  using_history();
  char *line;
  int result;

  for (;;) {
    line = readline(">>> ");
    if (!line)
      break;
    if (!*line) {
      free(line);
      continue;
    }

    add_history(line);
    initializeCompileState(&context, line);
    result = cb_compiler_resume(context.compile_state);
    free(line);

    if (context.did_init_vm) {
      // ...
    }

    if (result) {
      // Handle compilation error
      break;
    }

    if (!context.did_init_vm) {
      initializeVM(&context);
    }

    // ...
  }

  if (context.did_init_vm) {
    cb_vm_deinit();
  }

  cb_compile_state_free(context.compile_state);
  cb_bytecode_free(context.bytecode);
  cb_modspec_free(context.modspec);
}

int main() {
  repl();
  return 0;
}